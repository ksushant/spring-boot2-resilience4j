package com.falabella.logistics.inventory.dto;

import lombok.Data;

@Data
public class InventoryResponse {

    public String itemName;
    public String itemId;
    public String quantity;
}
