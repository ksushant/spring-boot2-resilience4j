package com.falabella.logistics.inventory.service;

import com.falabella.logistics.inventory.dto.InventoryResponse;
import com.falabella.logistics.inventory.repository.InventoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class InventoryService {

    @Autowired
    public InventoryRepository inventoryRepository;

    public InventoryResponse getInventoryForItem(boolean isException) throws Exception {

        if (isException) {
            log.info("Inventory : get Inventory for single item - Throwing exception");
            throw new Exception();
        } else {
            return inventoryRepository.getInventoryForItem();
        }
    }

    public List<InventoryResponse> getInventoryForItems(boolean isException) throws Exception {

        if (isException) {
            log.info("Inventory : get Inventory for multiple items - Throwing exception");
            throw new Exception();
        } else {
            return inventoryRepository.getInventoryForItems();
        }
    }
}
