package com.falabella.logistics.inventory.repository;

import com.falabella.logistics.inventory.dto.InventoryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class InventoryRepository {

    public InventoryResponse getInventoryForItem() {
        InventoryResponse inventoryResponse = new InventoryResponse();
        inventoryResponse.setItemId("I1");
        inventoryResponse.setItemName("Blue Shirt");
        inventoryResponse.setQuantity("5");
        return inventoryResponse;
    }

    public List<InventoryResponse> getInventoryForItems() {
        List<InventoryResponse> inventoryResponseList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            InventoryResponse inventoryResponse = new InventoryResponse();
            inventoryResponse.setItemId("I"+ i);
            inventoryResponse.setItemName("Blue Shirt");
            inventoryResponse.setQuantity("5");
            inventoryResponseList.add(inventoryResponse);
        }
        return inventoryResponseList;
    }



}
