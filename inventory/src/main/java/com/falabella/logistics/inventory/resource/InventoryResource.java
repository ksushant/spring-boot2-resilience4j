package com.falabella.logistics.inventory.resource;


import com.falabella.logistics.inventory.dto.InventoryResponse;
import com.falabella.logistics.inventory.repository.InventoryRepository;
import com.falabella.logistics.inventory.service.InventoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/inventory")
@Slf4j
public class InventoryResource {

    @Autowired
    public InventoryService inventoryService;

    @GetMapping("/items/{itemId}")
    public ResponseEntity<InventoryResponse> getInventoryForItem(@RequestParam(required = false) boolean isException)
            throws Exception {
        InventoryResponse inventoryResponse = inventoryService.getInventoryForItem(isException);
        return new ResponseEntity<>(inventoryResponse, HttpStatus.OK);
    }

    @GetMapping("/items")
    public ResponseEntity<List<InventoryResponse>> getInventoryForItems(@RequestParam(required = false) boolean isException)
            throws Exception {
        List<InventoryResponse> inventoryResponseList = inventoryService.getInventoryForItems(isException);
        return new ResponseEntity<>(inventoryResponseList, HttpStatus.OK);
    }
}
