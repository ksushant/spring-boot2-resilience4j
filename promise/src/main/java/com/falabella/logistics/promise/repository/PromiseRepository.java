package com.falabella.logistics.promise.repository;

import com.falabella.logistics.promise.dto.PromiseResponse;
import org.springframework.stereotype.Repository;

@Repository
public class PromiseRepository {

    public PromiseResponse getPromiseResponse (boolean isEmpty) {

        PromiseResponse promiseResponse = null;

        if (!isEmpty) {
            promiseResponse = new PromiseResponse();
            promiseResponse.setPromiseId("123456789");
        }

        return promiseResponse;
    }
}
