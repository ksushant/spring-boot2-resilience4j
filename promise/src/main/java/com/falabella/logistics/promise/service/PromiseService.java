package com.falabella.logistics.promise.service;

import com.falabella.logistics.promise.adapter.InventoryAdapter;
import com.falabella.logistics.promise.client.InventoryClient;
import com.falabella.logistics.promise.dto.InventoryResponse;
import com.falabella.logistics.promise.dto.PromiseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PromiseService {

    @Autowired
    public InventoryAdapter inventoryAdapter;

    public PromiseResponse getPromiseForSingleItem(boolean isException) throws Exception {
        PromiseResponse promiseResponse = inventoryAdapter.getPromiseForSingleItem(isException);
        return promiseResponse;
    }

    public PromiseResponse getPromiseForMultipleItems(boolean isException) throws Exception {
        PromiseResponse promiseResponse = inventoryAdapter.getPromiseForMultipleItems(isException);
        return promiseResponse;
    }
}
