package com.falabella.logistics.promise.dto;

import lombok.Data;

import java.util.List;

@Data
public class PromiseResponse {

   public String promiseId;
   List<InventoryResponse> inventoryResponseList;

}
