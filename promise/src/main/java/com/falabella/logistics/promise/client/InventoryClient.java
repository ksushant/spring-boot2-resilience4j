package com.falabella.logistics.promise.client;

import com.falabella.logistics.promise.dto.InventoryResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "inventory", url = "http://localhost:9081/")
public interface InventoryClient {

        @RequestMapping(method = RequestMethod.GET, value = "/inventory/items")
        List<InventoryResponse> getInventoryForItems(@RequestParam(required = false) boolean isException);

        @RequestMapping(method = RequestMethod.GET, value = "/inventory/items/2")
        InventoryResponse getInventoryForItem(@RequestParam(required = false) boolean isException);
}
