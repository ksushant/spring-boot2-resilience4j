package com.falabella.logistics.promise.adapter;

import com.falabella.logistics.promise.client.InventoryClient;
import com.falabella.logistics.promise.dto.InventoryResponse;
import com.falabella.logistics.promise.dto.PromiseResponse;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.falabella.logistics.promise.constants.PromiseConstants.INVENTORY;

@Component
@Slf4j
public class InventoryAdapter {

    @Autowired
    public InventoryClient inventoryClient;

    @CircuitBreaker(name = INVENTORY, fallbackMethod = "getInventorySingleItemFallback")
    public PromiseResponse getPromiseForSingleItem(boolean isException) {
        PromiseResponse promiseResponse = new PromiseResponse();
        promiseResponse.setPromiseId("123456789");
        log.info("Calling inventory to get promise for single item");
        InventoryResponse inventoryResponse = inventoryClient.getInventoryForItem(isException);
        log.info("Success Response from inventory service");
        List<InventoryResponse> inventoryResponseList = new ArrayList<>();
        inventoryResponseList.add(inventoryResponse);
        promiseResponse.setInventoryResponseList(inventoryResponseList);
        return promiseResponse;
    }

    @CircuitBreaker(name = INVENTORY, fallbackMethod = "getInventoryMultipleItemsFallback")
    public PromiseResponse getPromiseForMultipleItems(boolean isException) {
        PromiseResponse promiseResponse = new PromiseResponse();
        promiseResponse.setPromiseId("123456789");
        List<InventoryResponse> inventoryResponseList = inventoryClient.getInventoryForItems(isException);
        log.info("Success Response from inventory service");
        promiseResponse.setInventoryResponseList(inventoryResponseList);
        return promiseResponse;
    }

    private PromiseResponse getInventorySingleItemFallback(boolean isException, Exception ex) {
        log.info("Inside fallback of get promise for single items");
        PromiseResponse promiseResponse = new PromiseResponse();
        promiseResponse.setPromiseId("987654321");
        log.error("printing exception : {}", ex.getMessage());
        return promiseResponse;
    }

    private PromiseResponse getInventoryMultipleItemsFallback(boolean isException, Exception ex) {
        log.info("Inside fallback of get promise for multiple items");
        PromiseResponse promiseResponse = new PromiseResponse();
        promiseResponse.setPromiseId("987654321");
        log.error("printing exception : {}", ex.getMessage());
        return promiseResponse;
    }
}
