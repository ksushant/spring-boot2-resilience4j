package com.falabella.logistics.promise.resource;

import com.falabella.logistics.promise.dto.PromiseResponse;
import com.falabella.logistics.promise.repository.PromiseRepository;
import com.falabella.logistics.promise.service.PromiseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PromiseResource {

    @Autowired
    public PromiseService promiseService;

    @GetMapping("/promises")
    public ResponseEntity<PromiseResponse> getPromise(@RequestParam(required = false) boolean isSingleItem,
                                                      @RequestParam(required = false) boolean isException) {

        PromiseResponse promiseResponse = new PromiseResponse();
        try {
            if (isSingleItem) {
                promiseResponse = promiseService.getPromiseForSingleItem(isException);
            } else {
                promiseResponse = promiseService.getPromiseForMultipleItems(isException);
            }

        } catch (Exception e) {
            log.error("Error while getting promise for single item", e.getMessage());
            return new ResponseEntity<>(promiseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(promiseResponse, HttpStatus.OK);
    }
}
