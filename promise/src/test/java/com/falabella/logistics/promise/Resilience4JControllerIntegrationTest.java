/*
package com.falabella.logistics.promise;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

public class Resilience4JControllerIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @RepeatedTest(10)
    public void test(RepetitionInfo repetitionInfo) {
        int delay = 2;
        webTestClient.get()
                .uri("/promises", delay)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus()
                .isOk();
    }
}
*/
