# Resilience4j

<b>Resilience4j</b> is a Java library that helps us build resilient and fault-tolerant applications. It provides a framework for writing code to prevent and handle such issues.
Written for Java 8 and above, Resilience4j works on constructs like functional interfaces, lambda expressions, and method references.

## States and Settings

Can be in one of the three states:

- <b>CLOSED</b> – everything is fine, no short-circuiting involved.
- <b>OPEN</b> – remote server is down, all requests to it are short-circuited.
- <b>HALF_OPEN</b> – a configured amount of time since entering OPEN state has elapsed and CircuitBreaker allows requests to check if the remote service is back online.

## Resilience4j Modules and their purpose:

- <b>Retry:</b> Automatically retry a failed remote operation
- <b>RateLimiter:</b> Limit how many times we call a remote operation in a certain period
- <b>TimeLimiter:</b> Set a time limit when calling remote operation
- <b>Circuit Breaker:</b> Fail fast or perform default actions when a remote operation is continuously failing
- <b>Bulkhead:</b> Limit the number of concurrent remote operations
- <b>Cache:</b> Store results of costly remote operations


## Configuration details:

1. <b> Sliding-window-type:</b> Configures the type of the sliding window which is used to record the outcome of calls when the CircuitBreaker is closed. we will use a count-based circuit breaker type. In this type, the circuit will trip or move to an open state based on each incoming request.
2. <b>Sliding-window-size:</b> Configures the size of the sliding window which is used to record the outcome of calls when the CircuitBreaker is closed.
3. <b>Failure-rate-threshold:</b> it shows the percentage of the total sliding-window-size that fails and will cause the circuit breaker trips to open state. This means, with a configuration of 40%, 2 out of 5 failed requests will cause the circuit breaker trips to open state.
4. <b>Slow-call-rate-threshold:</b> it shows the percentage of the total sliding-window-size that fails which will cause the circuit breaker trips to open state. From the configuration above, it can be seen that 2 out of 5 failed requests will cause the circuit breaker trips to open state.
5. <b>Slow-call-duration-threshold:</b> is the time taken to indicate the received response exceeds this configuration time will be recorded as an error count.
6. <b>permittedNumberOfCallsInHalfOpenState:</b> Configures the number of permitted calls when the CircuitBreaker is half open.
7. <b>minimumNumberOfCalls:</b> Configures the minimum number of calls which are required (per sliding window period) before the CircuitBreaker can calculate the error rate or slow call rate.
8. <b>waitDurationInOpenState:</b> The time that the CircuitBreaker should wait before transitioning from open to half-open.

## How to configure Resilience4j in your service:

1. <b>Add the @CircuitBreaker annotation at the service method.</b>
2. <b>Provide the fallback method.</b>
3. <b>Add custom configurations in the yml file.</b>


- Annotation:
```
@CircuitBreaker(name = INVENTORY, fallbackMethod = "getInventorySingleItemFallback")
```
- Fallback method:

The fallback method name is fallbackProcess it should be in the same class and it should have the same signature but with an extra parameter for the Throwable class for the exception handling.
```
private PromiseResponse getInventorySingleItemFallback(boolean isException, Exception ex) {
  ........
}
```
- Configurations:
```
resilience4j.circuitbreaker:
  configs:
    default:
      slidingWindowSize: 1000
      permittedNumberOfCallsInHalfOpenState: 10
      waitDurationInOpenState: 10000
      failureRateThreshold: 60
      eventConsumerBufferSize: 10
      registerHealthIndicator: true
      recordExceptions:
        - java.lang.Exception
      ignoreExceptions:
  instances:
    inventory:
      baseConfig: default
      slidingWindowType: COUNT_BASED
      minimumNumberOfCalls: 15
      slidingWindowSize: 10000
      failureRateThreshold: 50
      waitDurationInOpenState: 1000
      permittedNumberOfCallsInHalfOpenState: 5
      eventConsumerBufferSize: 10
      registerHealthIndicator: true
```